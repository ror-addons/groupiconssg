<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Group Icons SG" version="2.3" date="5/10/2011" >
		<Author name="gutgut" email="" />
		<Description text="Adds icons above party and warband members so it is easier to find your teammates in the thick of battle!" />
		 <VersionSettings gameVersion="1.5" windowsVersion="1.5" savedVariablesVersion="1.4" /> 
         <Dependencies>
            <Dependency name="LibSlash" optional="true" forceEnable="true" />
            <Dependency name="LibAddonButton" optional="true" forceEnable="true" />
        </Dependencies>
		<Files>
			<File name="GroupIconsSG.lua" />
			<File name="GroupIconsSG.xml" />
		</Files>
		<OnInitialize>
			<CallFunction name="GroupIconsSG.OnInitialize" />
		</OnInitialize>
		<OnUpdate>
			<CallFunction name="GroupIconsSG.OnUpdate" />
		</OnUpdate>
		<OnShutdown>
			<CallFunction name="GroupIconsSG.OnShutdown" />
    	</OnShutdown>
    	<SavedVariables>
			<SavedVariable name="GroupIconsSG.Targets" />
			<SavedVariable name="GroupIconsSG.dim" />
			<SavedVariable name="GroupIconsSG.option" />
		</SavedVariables>
	</UiMod>
</ModuleFile>
