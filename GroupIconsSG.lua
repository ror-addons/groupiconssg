--[[
GroupIconsSG
]]--

local TIME_DELAY = 0.5
local timeLeft = TIME_DELAY

local deb = false

local NUM_WINDOWS = 24
local ICON_OFFSET_X = -50
local ICON_OFFSET_Y = 25

local ICON_GROUP = 66
local ICON_GROUP_DEAD = 68
local ICON_BATTLEGROUP = 110
local ICON_WARBANDLEADER = 150
local ICON_GROUPLEADER = 149
local ICON_MOB = 757
local ICON_PET = 4683
local ICON_TANK = 109
local ICON_TANK2 = 5211
local ICON_DD = 25
local ICON_DD2 = 4530
local ICON_HEAL = 191   ---119
local ICON_HEAL2 = 7900   ---119

local GROUPDIM = "GroupIconsSGOptionsSlider1"
local WARBANDDIM = "GroupIconsSGOptionsSlider3"
local MARKERDIM = "GroupIconsSGOptionsSlider2"
local HOSTILEDIM = "GroupIconsSGOptionsSlider4"
 
local WARBANDVIS = "GroupIconsSGOptionsChk1"
local HOSTILEVIS = "GroupIconsSGOptionsChk2"
local PETVIS = "GroupIconsSGOptionsChk3"
local LEADERVIS = "GroupIconsSGOptionsChk4"
local SELFLEADERVIS = "GroupIconsSGOptionsChk5"

GroupIconsSG = {}
GroupIconsSG.Targets = {}
GroupIconsSG.markers={}

GroupIconsSG.dim = {}
--[[
GroupIconsSG.dim[GROUPDIM]={scale=1,size=34,pos=16,bez=L"Group"}       	--gruppe
GroupIconsSG.dim[MARKERDIM]={scale=0.6,size=66,pos=32,bez=L"Special"}       --marker
GroupIconsSG.dim[WARBANDDIM]={scale=0.6,size=34,pos=16,bez=L"Warband"}       --wb ander grps
GroupIconsSG.dim[HOSTILEDIM]={scale=0.4,size=66,pos=32,bez=L"Enemy & Pet"}       --feind und pet
]]--


GroupIconsSG.dim[GROUPDIM]={scale=1,size=34,pos=16,bez=L"Group"}       	--gruppe
GroupIconsSG.dim[WARBANDDIM]={scale=0.6,size=66,pos=32,bez=L"Warband"}       --marker
GroupIconsSG.dim[MARKERDIM]={scale=0.6,size=66,pos=16,bez=L"Special"}       --wb ander grps
GroupIconsSG.dim[HOSTILEDIM]={scale=0.4,size=66,pos=32,bez=L"Enemy & Pet"}       --feind und pet


GroupIconsSG.option = {}
GroupIconsSG.option[WARBANDVIS]={visible=true,bez=L"Warband"}
GroupIconsSG.option[HOSTILEVIS]={visible=true,bez=L"Hostile Target";}
GroupIconsSG.option[PETVIS]={visible=true,bez=L"Pet";}
GroupIconsSG.option[LEADERVIS]={visible=true,bez=L"Leader";}
GroupIconsSG.option[SELFLEADERVIS]={visible=true,bez=L"IamTheleader";}


local tint = {}
tint["white"]={255, 255, 255}
tint["red"]={255, 0, 0} 
tint["gray"]={100, 100, 100} 
tint["dred"]={155, 0, 0}  
tint["yellow"]={255, 255, 0} 

marker={}
marker.lastObjNum=0
marker.size=1
marker.color={255,255,255}
marker.texture=0
marker.name=""	



function marker:Attach(id)

	if ( self.lastObjNum ~= id ) then
		if ( self.lastObjNum ~= nil ) then
			DetachWindowFromWorldObject(self.name, self.lastObjNum)
		end
		AttachWindowToWorldObject(self.name, id)
		self.lastObjNum = id
	end

end

function marker:Size(size)

	--if ( self.size ~= size ) then
		WindowSetScale(self.name,GroupIconsSG.dim[size].scale)
		WindowSetDimensions(self.name .. "Icon",GroupIconsSG.dim[size].size,GroupIconsSG.dim[size].size)
		self.size = size
	--end
    
end

function marker:Color(tintnr)

	if ( self.color ~= tintnr ) then
        WindowSetTintColor(self.name, tint[tintnr][1],tint[tintnr][2],tint[tintnr][3])
		self.color = tintnr
	end

end

function marker:ColorBG(tintnr)

	if ( self.colorbg ~= tintnr ) then
        WindowSetTintColor(self.name.."Background", tint[tintnr][1],tint[tintnr][2],tint[tintnr][3])
		self.colorbg = tintnr
	end

end

function marker:Texture(texture,move)

	
	if not move then move=1 end
	if ( self.texture ~= texture ) then
		CircleImageSetTexture(self.name .. "Icon", GetIconData(texture), GroupIconsSG.dim[move].pos, GroupIconsSG.dim[move].pos)						   
		self.texture = texture
	end

end

function marker:Offset()

		WindowSetOffsetFromParent(self.name.."Icon", ICON_OFFSET_X, ICON_OFFSET_Y)

end

function marker:Alpha(alpha)

	if ( self.alpha ~= alpha ) then
		WindowSetAlpha(self.name, alpha)					   
		self.alpha = alpha
	end

end

function marker:AlphaBG(alpha)

	if ( self.alphabg ~= alpha ) then
		WindowSetAlpha(self.name.."Background", alpha)  
		self.alphabg = alpha
	end

end

function marker:Create(on)

	if ( self.on ~= on ) then
		if on then
			CreateWindowFromTemplate(self.name, "GroupIconsSGWindow", "Root")  
	    else
	        DestroyWindow(self.name)
		end
		self.on = on
	end

end

function marker:Show(show)

	if ( self.show ~= show ) then
		WindowSetShowing(self.name, show)
		self:Alpha(0)  
		self.show = show
	end

end

function marker:neu()
	local o = {} 
    setmetatable (o, { __index = self } )	
    return o 
end

function formName(SpielerName)

	if (not SpielerName) then return L""; end; 													
	if type(SpielerName) == "string" then														
		SpielerName = towstring(SpielerName)													
	end
	SpielerName = wstring.gsub( SpielerName, L"(^.)", L"" )										
	return tostring(SpielerName)																
end
function GroupIconsSG.SlashHandler(input)

   WindowSetShowing("GroupIconsSGOptions",not WindowGetShowing("GroupIconsSGOptions"))
	
end
function GroupIconsSG.OnInitialize()
	
	LibSlash.RegisterSlashCmd("gsg", function(input) GroupIconsSG.SlashHandler(input) end)
	
	if (not isLibAddonButtonRegistered) then
		if (LibAddonButton) then
			LibAddonButton.Register("fx");
			LibAddonButton.AddMenuItem("fx", "Group Icons SG", GroupIconsSG.SlashHandler);
			isLibAddonButtonRegistered = true;
		end
	end
	
	local windowR="GroupIconsSGOptions";
	CreateWindow(windowR, false)
	LayoutEditor.RegisterWindow( windowR,L"GroupIconsSGOptions",L"GSG Settings",false, false,true,nil)
	WindowSetFontAlpha (windowR, 0.5);
	LabelSetText( windowR.."TextName", L"GroupIconsSG Options" )
	WindowSetFontAlpha (windowR.."TextName", 0.5);
	LabelSetTextColor(windowR.."TextName", 255, 255 , 255 );
	
	for k,v in pairs(GroupIconsSG.dim) do
		SliderBarSetCurrentPosition( k, v.scale )
		LabelSetText( k.."Text", v.bez )
    
 	end
	
	for k,v in pairs(GroupIconsSG.option) do
		ButtonSetPressedFlag( k,v.visible )
		LabelSetText( k.."Text", v.bez )
   	end
	
	
	for i = 1, NUM_WINDOWS do
		GroupIconsSG.markers[i]=marker:neu()
		GroupIconsSG.markers[i].name="GroupIconsSGWindow" .. i
		GroupIconsSG.markers[i]:Create(true)
		GroupIconsSG.markers[i]:Show(true)
	end
	
        GroupIconsSG.markers["E"]=marker:neu()
		GroupIconsSG.markers["E"].name="GroupIconsSGWindowE"
		GroupIconsSG.markers["E"]:Create(true)
		GroupIconsSG.markers["E"]:Show(true)
		
		GroupIconsSG.markers["F"]=marker:neu()
		GroupIconsSG.markers["F"].name="GroupIconsSGWindowF"
		GroupIconsSG.markers["F"]:Create(true)
		GroupIconsSG.markers["F"]:Show(true)
		
		GroupIconsSG.markers["Pet"]=marker:neu()
		GroupIconsSG.markers["Pet"].name="GroupIconsSGWindowPet"
		GroupIconsSG.markers["Pet"]:Create(true)
		GroupIconsSG.markers["Pet"]:Show(true)
		
end


function GroupIconsSG.OnShutdown()
	for i = 1, GroupIconsSG.NUM_WINDOWS do
		GroupIconsSG.markers[i]:Create(false)
	end
end

function GroupIconsSG.OptionsSliderMove(val)

	GroupIconsSG.dim[SystemData.ActiveWindow.name].scale= val

end

function GroupIconsSG.OptionsCHKclick(val)

	GroupIconsSG.option[SystemData.ActiveWindow.name].visible= not GroupIconsSG.option[SystemData.ActiveWindow.name].visible
	ButtonSetPressedFlag( SystemData.ActiveWindow.name,GroupIconsSG.option[SystemData.ActiveWindow.name].visible )
    	d("VALUEPRESS",val,ButtonGetPressedFlag(SystemData.ActiveWindow.name))

end


function GroupIconsSG.PinGroup()
	return PartyUtils.IsPartyMemberValid(1)
end

function GroupIconsSG.OnUpdate( elapsed )

	timeLeft = timeLeft - elapsed
	if timeLeft > 0 then
		return 
	end
	timeLeft = TIME_DELAY 
    
	for i = 1, NUM_WINDOWS do
		GroupIconsSG.markers[i].tohide = true
	end

	local grpsrch={}
	for k = 1, table.getn(GetGroupData()) do
		if ( GetGroupData()[k].worldObjNum ~= 0 ) then
	         grpsrch[GetGroupData()[k].worldObjNum]=true
		end
	end

	local pet = GameData.Player.Pet.objNum
	if pet ~= 0 and GroupIconsSG.option[PETVIS].visible then
		local m =GroupIconsSG.markers["Pet"]
		m:Attach(pet)
		m:ColorBG("yellow")
	    m:Size(HOSTILEDIM)
		m:Texture(ICON_PET,HOSTILEDIM)                    
		m:Alpha(1)
		m:AlphaBG(1)
	else
		local m =GroupIconsSG.markers["Pet"]
        m:Alpha(0)			
	end

	local ht = TargetInfo:UnitEntityId("selfhostiletarget")
	if ht ~= 0 and GroupIconsSG.option[HOSTILEVIS].visible then
		local m =GroupIconsSG.markers["E"]
		m:Attach(ht)
		
		m:ColorBG("yellow")
        local tname = formName(TargetInfo:UnitName("selfhostiletarget"))
		if GroupIconsSG.Targets[tname] then
			m:Size(MARKERDIM)
			m:Texture(GroupIconsSG.Targets[tname],MARKERDIM)
		elseif TargetInfo:UnitIsNPC("selfhostiletarget") then
	   	    m:Size(HOSTILEDIM)
			m:Texture(ICON_MOB,HOSTILEDIM)
		elseif TargetInfo:UnitType ("selfhostiletarget")== 5 then
			m:Size(GROUPDIM)	
			m:Texture(Icons.GetCareerIconIDFromCareerLine( TargetInfo:UnitCareer("selfhostiletarget") ),GROUPDIM)
		else
			m:Size(HOSTILEDIM)
			m:Texture(946,HOSTILEDIM)
		end
		m:Alpha(1)
		m:AlphaBG(1)
 	else
		local m =GroupIconsSG.markers["E"]
        m:Alpha(0)			
	end

       
	if ( IsWarBandActive()  and not GameData.Player.isInScenario and not GameData.Player.isInSiege) then
		if deb then d("warband") end
		local workingNumber = 1
    		for i = 1, table.getn(GetBattlegroupMemberData()) do

			if ( GetBattlegroupMemberData()[i].players ~= nil ) then
					
				for j = 1, table.getn(GetBattlegroupMemberData()[i].players) do
					local m =GroupIconsSG.markers[workingNumber]
					local member = GetBattlegroupMemberData()[i].players[j]
					local isself = member.worldObjNum == GameData.Player.worldObjNum
					if ( member.worldObjNum ~= 0 and (not isself or GroupIconsSG.option[SELFLEADERVIS].visible) ) then
						m.tohide = false
						m:Attach(member.worldObjNum)
						if not isself then
							m:Alpha(1)
							m:AlphaBG(1)
						end

						local inGroup = false
						for k = 1, table.getn(GetGroupData()) do
							if ( GetGroupData()[k].worldObjNum == member.worldObjNum ) then
								inGroup = true
								k = table.getn(GetGroupData())+1 -- break
							end
						end
						--if grpsrch[member.worldObjNum] then 
						--	newd(1,"dasamma",inGroup) 
						--	inGroup = true
						--end
						
						if not GroupIconsSG.option[WARBANDVIS].visible and not inGroup then m:Alpha(0) end

						if ( member.healthPercent == 0 ) then
						    m:Color("red")
						else
						    m:Color("white")
						end
						
						local tname = formName(member.name)
						if GroupIconsSG.Targets[tname] then
							m:Size(MARKERDIM)
							m:Texture(GroupIconsSG.Targets[tname],MARKERDIM)
						elseif member.isGroupLeader and GroupIconsSG.option[LEADERVIS].visible then
							m:AlphaBG(0)
							m:Size(HOSTILEDIM)
							m:Texture(ICON_WARBANDLEADER,HOSTILEDIM)
							if GroupIconsSG.option[SELFLEADERVIS].visible then m:Alpha(1) end
						else
                        	if ( inGroup ) then
								m:Size(GROUPDIM)
							else
								if ( member.healthPercent == 0 ) then
								    m:Color("dred")
								else
								    m:Color("gray")
								end
								m:Size(WARBANDDIM)
							end
							m:Texture(Icons.GetCareerIconIDFromCareerLine( member.careerLine ),GROUPDIM)
						end
						--m:Offset()
					else
						m:Alpha(0)
					end
					workingNumber = workingNumber + 1
				end
				
				
				
			end
   		end
	elseif ( GroupIconsSG.PinGroup() ) then
		if deb then d("gruppe") end
		local workingNumber = 1
		for i = 1, table.getn(GetGroupData()) do
			local member = GetGroupData()[i]
			local m =GroupIconsSG.markers[workingNumber]
			
			if ( member.worldObjNum ~= 0 ) and (not isself or GroupIconsSG.option[SELFLEADERVIS].visible)  then
                        m.tohide = false
				        m:Attach(member.worldObjNum)
					    m:Alpha(1)
					    m:AlphaBG(1)
					    
						if ( member.healthPercent == 0 ) then
						    m:Color("red")
						else
						    m:Color("white")
						end
												
						local tname = formName(member.name)
						if GroupIconsSG.Targets[tname] then
							m:Size(MARKERDIM)
							m:Texture(GroupIconsSG.Targets[tname],MARKERDIM)
						elseif member.isGroupLeader  and GroupIconsSG.option[LEADERVIS].visible then
						    m:Size(HOSTILEDIM)
							m:Texture(ICON_GROUPLEADER,HOSTILEDIM)	
						else								
							m:Size(GROUPDIM)
							m:Texture(Icons.GetCareerIconIDFromCareerLine( member.careerLine ),GROUPDIM)
						end	

				--m:Offset()
			else
				m:Alpha(0)
			end
			
			workingNumber = workingNumber + 1
			
		end
	
	end
	
    for i = 1, NUM_WINDOWS do
		if GroupIconsSG.markers[i].tohide then GroupIconsSG.markers[i]:Alpha(0) end
	end
	
	
end



function GroupIconsSG.SetTANK(iid)
	if not iid then iid = ICON_TANK end
	local name=formName(TargetInfo:UnitName(TargetInfo.FRIENDLY_TARGET)) 
	if name > "" then GroupIconsSG.Targets[name]=iid end
end

function GroupIconsSG.SetTANK2(iid)
	if not iid then iid = ICON_TANK2 end
	local name=formName(TargetInfo:UnitName(TargetInfo.FRIENDLY_TARGET)) 
	if name > "" then GroupIconsSG.Targets[name]=iid end
end

function GroupIconsSG.SetDD(iid)
	if not iid then iid = ICON_DD end
	local name=formName(TargetInfo:UnitName(TargetInfo.FRIENDLY_TARGET)) 
	if name > "" then GroupIconsSG.Targets[name]=iid end
end

function GroupIconsSG.SetDD2(iid)
	if not iid then iid = ICON_DD2 end
	local name=formName(TargetInfo:UnitName(TargetInfo.FRIENDLY_TARGET)) 
	if name > "" then GroupIconsSG.Targets[name]=iid end
end

function GroupIconsSG.SetHEAL(iid)
	if not iid then iid = ICON_HEAL end
	local name=formName(TargetInfo:UnitName(TargetInfo.FRIENDLY_TARGET)) 
	if name > "" then GroupIconsSG.Targets[name]=iid end
end

function GroupIconsSG.SetHEAL2(iid)
	if not iid then iid = ICON_HEAL2 end
	local name=formName(TargetInfo:UnitName(TargetInfo.FRIENDLY_TARGET)) 
	if name > "" then GroupIconsSG.Targets[name]=iid end
end

function GroupIconsSG.SetHostile(iid)
	if not iid then iid = ICON_HEAL2 end
	local name=formName(TargetInfo:UnitName(TargetInfo.HOSTILE_TARGET)) 
	if name > "" then GroupIconsSG.Targets[name]=iid end
end

function GroupIconsSG.Remove()
	local name=formName(TargetInfo:UnitName(TargetInfo.FRIENDLY_TARGET))
	if name > "" then GroupIconsSG.Targets[name]=nil end
end

function GroupIconsSG.RemoveHostile()
	local hname=formName(TargetInfo:UnitName(TargetInfo.HOSTILE_TARGET))  
	if hname > "" then GroupIconsSG.Targets[hname]=nil end
end 


function GroupIconsSG.Reset()
	GroupIconsSG.Targets={}
end